# Setup

## Docker swarm on Windows with hyperv

Create external vswitch so your docker-machine instances get real routable IPs.

```
Import-Module Hyper-V
Get-NetAdapter
$adaptor = Get-NetAdapter -Name WiFi
New-VMSwitch -Name DockerNat -NetAdapterName $adaptor.Name -AllowManagementOS $true -Notes 'For docker-machines'
```

Create docker machines
Refer to https://github.com/docker/labs/tree/master/swarm-mode/beginner-tutorial
```
docker-machine create --driver hyperv --hyperv-virtual-switch DockerNat manager1
docker-machine create --driver hyperv --hyperv-virtual-switch DockerNat worker1
```

Swarm init and tokens
```
$manager1ip = docker-machine ip manager1
docker-machine ssh manager1 "docker swarm init --listen-addr $manager1ip --advertise-addr $manager1ip"

$managertoken = docker-machine ssh manager1 "docker swarm join-token manager -q"
$workertoken = docker-machine ssh manager1 "docker swarm join-token worker -q"
```

We are only joining one worker, so we use the worker token
```
$worker1ip = docker-machine ip worker1
docker-machine ssh worker1 "docker swarm join --token $workertoken --listen-addr $worker1ip --advertise-addr $worker1ip $manager1ip"
docker-machine ssh manager1 "docker node ls"
```

Set the docker CLI to a machine
```
docker-machine env manager1 | Invoke-Expression
docker node ls
```

When you are done you can remove docker machines
```
docker-machine rm manager1
docker-machine rm worker1
```

# Launching one container per node

Here we can manipulate our service using `--mode global` to run one copy of the service one each docker machine, exposed directly via the machine's ip.

```
docker service create --mode global --restart-condition any --name global-nginx --publish 80:80 nginx:1.10.3-alpine
```

Check nginx and version header
```
$(iwr $worker1ip).Headers.Server
$(iwr $manager1ip).Headers.Server
```

Apply rolling update
```
docker service update global-nginx --image nginx:1.11.3-alpine
docker service inspect --pretty global-nginx
```

Check nginx and version header
```
$(iwr $worker1ip).Headers.Server
$(iwr $manager1ip).Headers.Server
```

Kill on the worker and watch it respawn
```
docker-machine ssh worker1
docker ps
docker kill `docker ps --format '{{.ID}}'`
docker ps
```

## Docker CLI to API for the update

Checking the dockerd log we see the API calls resulting from `docker service update global-nginx --image nginx:1.10.3-alpine`

```
time="2017-04-14T02:10:56.902966375Z" level=debug msg="Calling GET /_ping"
time="2017-04-14T02:10:56.903828974Z" level=debug msg="Calling GET /v1.29/services/global-nginx"

{
	"ID": "zq2jzcvcwkvm6vkgo92aqe84m",
	"Version": {
		"Index": 11
	},
	"CreatedAt": "2017-04-28T08:06:09.0694582Z",
	"UpdatedAt": "2017-04-28T08:06:09.0747488Z",
	"Spec": {
		"Name": "global-nginx",
...

time="2017-04-14T02:10:56.905471074Z" level=debug msg="Calling POST /v1.29/services/gacdavgrcz1h5k5i4eefjp22g/update?registryAuthFrom=spec&version=225"
time="2017-04-14T02:10:56.905586874Z" level=debug msg="form data: {\"EndpointSpec..."
{
	"EndpointSpec": {
		"Mode": "vip",
		"Ports": [{
				"Protocol": "tcp",
				"PublishMode": "ingress",
				"PublishedPort": 80,
				"TargetPort": 80
			}
		]
	},
	"Mode": {
		"Global": {}
	},
	"Name": "global-nginx",
	"TaskTemplate": {
		"ContainerSpec": {
			"DNSConfig": {},
			"Image": "nginx:1.10.3-alpine"
		},
		"ForceUpdate": 0,
		"Placement": {},
		"Resources": {
			"Limits": {},
			"Reservations": {}
		},
		"RestartPolicy": {
			"Condition": "any",
			"MaxAttempts": 0
		}
	},
	"UpdateConfig": {
		"FailureAction": "pause",
		"MaxFailureRatio": 0,
		"Parallelism": 1
	}
}
```
$env:DOCKER_CERT_PATH

```
curl --insecure -v --cacert ca.pem --cert cert.pem --key key.pem https://10.9.83.141:2376/v1.24/images/json
```

```
curl --insecure -v --cacert ca.pem --cert cert.pem --key key.pem https://10.9.83.141:2376/v1.29/services/global-nginx
```

/v1.29/services/global-nginx
